# Django-cms 搭建的示例中文站点

## Refer

- 根据此示例[Introductory Tutorial](http://docs.django-cms.org/en/2.4.3/getting_started/tutorial.html)

## Feature

- 设置了中文界面，测试文件，视频等插件可行
- 在Ubuntu Server上搭建

## License
Apache License version 2
